//
//  FavoritesCollectionViewCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/8/22.
//




import UIKit
import Kingfisher

protocol FavoritesDelegate: NSObjectProtocol {
    
    func reloadData(_ view: FavoritesCollectionViewCell)
}

class FavoritesCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    private var info: ItunesDataModel?
    weak var delegate: FavoritesDelegate?
    // MARK: - UI Component
    private let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        return view
    }()
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        return imageView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_B(14), color: .black, alignment: .left)
        label.numberOfLines = 2
        return label
    }()
    private let categoryLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(12), color: .rgb(129), alignment: .left)
        label.numberOfLines = 0
        return label
    }()
    private let ratingLabel: BaseButton = {
        let btn = BaseButton()
        btn.image(normal: "listStar")
        btn.title("", color: .rgb(129), size: 11, alignment: .right, isBold: false)
        btn.alignHorizontal(spacing: 2)
        
        return btn
    }()
    private let favBtn: BaseButton = {
        let btn = BaseButton()
        btn.image(normal: "heart2")
        btn.title("", color: .rgb(129), size: 11, alignment: .center, isBold: false)
        btn.tag = 1
        return btn
    }()
    private let newImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        imageView.image = UIImage(named: "badgeNew")
        return imageView
    }()
    private let recomendImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        imageView.image = UIImage(named: "badgeFeatured")
        return imageView
    }()
    private let recomendLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(8), color: .white, alignment: .center)
        label.text = ""
        label.isHidden = true
        return label
    }()
    private let offImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        imageView.image = UIImage(named: "badgeOff")
        return imageView
    }()
    private let offLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(11), color: .white, alignment: .center)
        label.numberOfLines = 2
        return label
    }()
    private let freeFeeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        imageView.image = UIImage(named: "badgeDvfree")
        return imageView
    }()
    
    // MARK: - Override
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetting()
        contentView.addSubview(containerView)
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(categoryLabel)
        containerView.addSubview(ratingLabel)
        containerView.addSubview(newImageView)
        containerView.addSubview(favBtn)
        containerView.addSubview(recomendImageView)
        containerView.addSubview(recomendLabel)
        containerView.addSubview(offImageView)
        containerView.addSubview(offLabel)
        containerView.addSubview(freeFeeImageView)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Internal Method
    
    func configure(_ list: ItunesDataModel) {
        if let imageUrl = list.artworkUrl100, let url = URL(string: imageUrl) {
            
            if favoritesData != nil {
                for i in favoritesData! {
                    if i.trackName == list.trackName {
                        favBtn.tag = 2
                        favBtn.image(normal: "Heart")
                    }
                }
            }
            let image = UIImage(named: "Apple" )
            let resource = ImageResource(downloadURL: url)
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: resource,placeholder: image,options: [.transition(.fade(0.3)),.memoryCacheExpiration(.days(3))])
        }
        
        self.info = list
        titleLabel.text = list.trackName ?? list.collectionName
        categoryLabel.text = "\(list.trackPrice ?? 0)"
        ratingLabel.setTitle("\(list.trackCount ?? 0)", for: .normal)
      
 
       
    }
    
    // MARK: - Private Method
    
    private func initialSetting() {
        backgroundColor = .white
        clipsToBounds = true
        layer.masksToBounds = false
        layer.cornerRadius = 10
        layer.shadowColor = .rgb(0, 0.1)
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 4
        
        favBtn.addTarget(self, action: #selector(clickFav), for: .touchUpInside)
    }
    
    // MARK: - Selector

   
    @objc func clickFav() {
        if favBtn.tag == 1 {
            favBtn.image(normal: "Heart")
            
            var selectedMenu = [ItunesDataModel]()
          
            selectedMenu.append(info!)
        
            returnConfigure(selected: selectedMenu)
        }else {
            
            let trackID = info?.trackName ?? info?.collectionName
            for i in favoritesData! {
                //audiobook remove favorites
                if i.collectionName == trackID {
                    
                    favBtn.image(normal: "heart2")
                    favoritesData?.removeAll(where: {$0.collectionName == trackID})
                    delegate?.reloadData(self)
                }
                //songs, movies remove favorites
                if i.trackName == trackID {
                    
                    favBtn.image(normal: "heart2")
                    favoritesData?.removeAll(where: {$0.trackName == trackID})
                    delegate?.reloadData(self)
                }
            }
            
        }
        
        
      
    }
    
    
    func returnConfigure(selected: [ItunesDataModel]) {
        print("HERE")
        guard let sel = favoritesData else {
            favoritesData = selected
            //menuViewReload()
          
            do {
             
           
            //    insertData(data: data, key: self.info!.name!)
            }catch {
                print(error.localizedDescription)
            }
          //  view.reloadInputViews()
            return
        }
        let selectedMenu = sel + selected
        favoritesData = selectedMenu
        print("WEWEADASC")
    
        
        if sel.contains(where: {$0.artistName == selected[0].artistName}) {
            var selectedMenu = [ItunesDataModel]()
            for i in 0 ..< sel.count {
                if let hashKey = sel[i].artistName, hashKey != "" {
                    if hashKey == selected[0].artistName {
                        var newSel = sel[i]
                        let selCount = sel[i].trackCount ?? 0
                        let addCount = selected[0].trackCount ?? 0
                        let newCount = selCount + addCount
                        newSel.trackCount = newCount
                        selectedMenu.append(newSel)
                    } else {
                        selectedMenu.append(sel[i])
                    }
                }
            }
            favoritesData = selectedMenu
          
        } else {
            let selectedMenu = sel + selected
            favoritesData = selectedMenu
        }
     
       print(selectedMenu)
  
       
      //  calculationView.isHidden = true
        
    }
}

//MARK: - Layout
extension FavoritesCollectionViewCell {
    
    private func layout() {
        containerView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        imageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        favBtn.topAnchor.constraint(equalTo: imageView.topAnchor,constant: 20).isActive = true
        favBtn.trailingAnchor.constraint(equalTo: imageView.trailingAnchor,constant: -20).isActive = true
        favBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        favBtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 11).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: ratingLabel.leadingAnchor, constant: -10).isActive = true
        
        categoryLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4).isActive = true
        categoryLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10).isActive = true
        categoryLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        
        ratingLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        ratingLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        ratingLabel.widthAnchor.constraint(equalToConstant: 34).isActive = true
        
       
    }
}
