//
//  FavoritesViewController.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/5/22.
//

import UIKit

class FavoritesViewController: BaseViewController {
 
    // MARK: - Properties
    private var info: [ItunesDataModel]?
    private var banners: [HomeBanner]?
  
    private var categHeight: NSLayoutConstraint!
  
    // MARK: - UI Component
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    private let scrollContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    private let collectionViews: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 165*screen_width/375, height: 250*screen_height/667)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
       
        collectionView.isPagingEnabled = false
        collectionView.register(cellType: FavoritesCollectionViewCell.self)
        return collectionView
    }()
    // MARK: - Override
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.setContentOffset(CGPoint(x: 20, y: 20), animated: true)
     
      
        view.addSubview(collectionViews)
 
       self.setNavTitle(title: "Favorites")
        showBackButton()
        initialSetting()
    
        layout()
        self.info = favoritesData
        
      
        viewControllerVisited = "Favorite"
      
        //Open previous page
        if selectedData != nil {
            let time = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: time) {
            let vc = SeeMoreViewController()
         
            vc.itunesData = selectedData!
           
                self.show(vc)
            }
        }
      
    }
    override func viewWillAppear(_ animated: Bool) {
        collectionViews.delegate = self
        collectionViews.dataSource = self
        collectionViews.reloadData()
        
        //Insert Favorites data
        
        self.info = favoritesData

        
    }
     

    // MARK: - Private Method

    private func initialSetting() {
        collectionViews.delegate = self
        collectionViews.dataSource = self
       
        reloadDelegate()
      
       
        collectionViews.reloadData()
      
    }
    func setNavTitle(title: String) {
       
       let containerView: UIView = {
           let view = UIView()
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       let navTitleLabel: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
           label.text = title
           label.textAlignment = .center
           label.font = .godo_B(16)
           label.textColor = .black
           label.numberOfLines = 0
           return label
       }()
       
    
       
       containerView.addSubview(navTitleLabel)
     
       
       containerView.widthAnchor.constraint(equalToConstant: screen_width/2).isActive = true
       containerView.heightAnchor.constraint(equalToConstant: navigationController?.navigationBar.frame.height ?? 48).isActive = true
       
       navTitleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
       navTitleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
       navTitleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
       
   
       
      
       
       setTitleView(view: containerView)
   }
    // MARK: - Selector
   
    
    @objc func clickFilter(sender : UITapGestureRecognizer) {
        
    }
    func reloadDelegate() {
       collectionViews.reloadData()
    }
}

//MARK: - LayOut
extension FavoritesViewController {
    
    private func layout() {
        
        let margins = view.safeAreaLayoutGuide
        
   
            
    
   
       
        collectionViews.topAnchor.constraint(equalTo: margins.topAnchor,constant: 10).isActive = true
        collectionViews.leadingAnchor.constraint(equalTo: margins.leadingAnchor,constant: 0).isActive = true
        collectionViews.trailingAnchor.constraint(equalTo: margins.trailingAnchor,constant: 0).isActive = true
     
 
        collectionViews.bottomAnchor.constraint(equalTo: margins.bottomAnchor,constant: 10).isActive = true
       // collectionView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor,constant: 20).isActive = true
      //  collectionView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
       
    }
    @objc private func didClickSearchButton() {
        
     

    }
  
    
    private func getDetailMenu(compKey: String, category: String? = nil) {
        
      
    }
 
}

//MARK: - CollectionViewDataSource
extension FavoritesViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = info?.count else{return 0}
       
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: FavoritesCollectionViewCell.self)
       // guard let compList = list else{return cell}
        
        guard let indexInfo = info?[indexPath.row] else{return cell}
        cell.delegate = self
        cell.configure(indexInfo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

//MARK: - CollectionViewDelegate
extension FavoritesViewController: UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailsViewController()
      
        vc.iTunesData = info![indexPath.row]
        show(vc)
    }
    

}


// MARK: - FavoritesDelegate
extension FavoritesViewController: FavoritesDelegate {
    func reloadData(_ view: FavoritesCollectionViewCell) {
        
        info = []
        collectionViews.reloadData()
        
        self.info = favoritesData
      
    }
    
    
    
}
