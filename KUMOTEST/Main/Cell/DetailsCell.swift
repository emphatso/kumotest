//
//  DetailsCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/7/22.
//


import UIKit
import AVFoundation
import WebKit

protocol DetailsViewDelegate: NSObjectProtocol {
    func clickPlayer(_ view: DetailsCell, previewUrl: String)
    func clickImage(_ view: DetailsCell, previewUrl: String)
}

class DetailsCell: UITableViewCell {

    // MARK: - Properties
    private var data : ItunesDataModel?
    weak var delegate: DetailsViewDelegate?
    var countdownTimer: Timer!
    var totalTime = 179
    var player : AVPlayer?
    // MARK: - UI Component
    private let btnPlay: BaseButton = {
        let btn = BaseButton()
        btn.image(normal: "playBtn")
        btn.tag = 2
        btn.backgroundColor = .white
        btn.roundCorners(radius: 25, corners: .allCorners)
        return btn
    }()
    private let trackNameLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .boldSystemFont(ofSize: 12), color: .white, alignment: .left)
        label.numberOfLines = 0
        label.text = "12:21"
        return label
    }()
    private let artisLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(12), color: .lightGray, alignment: .left)
        label.numberOfLines = 0
        label.text = "12:21"
        return label
    }()
    private let webView: WKWebView = {
        let contentController = WKUserContentController()
        let config = WKWebViewConfiguration()

        let viewPortScript = """
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'viewport');
            meta.setAttribute('content', 'width=device-width');
            meta.setAttribute('initial-scale', '1.0');
            meta.setAttribute('maximum-scale', '1.0');
            meta.setAttribute('minimum-scale', '1.0');
            meta.setAttribute('user-scalable', 'no');
            document.getElementsByTagName('head')[0].appendChild(meta);
        """
        let script = WKUserScript(source: viewPortScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        contentController.addUserScript(script)
        config.preferences.javaScriptEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        config.selectionGranularity = .character
        config.userContentController = contentController
        
        let web = WKWebView(frame: .zero, configuration: config)
        web.translatesAutoresizingMaskIntoConstraints = false
        web.scrollView.showsVerticalScrollIndicator = false
        web.scrollView.isScrollEnabled = false
        return web
    }()
    private let musicView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.shadowColor = .rgb(0, 0.15)
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 8
     
        view.layer.masksToBounds = true
        view.clipsToBounds = true
        return view
    }()
    private let playerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()
    private let imgView: UIImageView = {
        let imageView = UIImageView()
         imageView.translatesAutoresizingMaskIntoConstraints = false
         imageView.contentMode = .scaleToFill
         imageView.isUserInteractionEnabled = true
         imageView.clipsToBounds = false
         imageView.image = UIImage(named: "playBtn")
         imageView.backgroundColor = .white
        return imageView
    }()
    private let trailerLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .boldSystemFont(ofSize: 15), color: .white, alignment: .left)
        label.numberOfLines = 0
        label.text = "Trailer"
        return label
    }()
    private let timeLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(12), color: .white, alignment: .left)
        label.numberOfLines = 0
        label.text = "12:21"
        return label
    }()
    private let aboutLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .boldSystemFont(ofSize: 15), color: .white, alignment: .left)
        label.numberOfLines = 0
        return label
    }()
    private let longDescLbl: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(14), color: .white, alignment: .left)
        label.numberOfLines = 0
        return label
    }()
    private let ratingLabel: BaseButton = {
        let btn = BaseButton()
        btn.image(normal: "listStar")
        btn.title("", color: .white, size: 11, alignment: .left, isBold: false)
        btn.alignHorizontal(spacing: 4)
        return btn
    }()
    private let reviewLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(11), color: .rgb(129), alignment: .left)
        return label
    }()
    private let exLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(11), color: .rgb(129), alignment: .left)
        return label
    }()

    
    // MARK: - Override
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initialSetting()
       
  
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Private Method
    
    private func initialSetting() {
        btnPlay.addTarget(self, action: #selector(clickPlay), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    // MARK: - Selector

    
    func startTimer() {
        let time = Double(data!.trackTimeMillis ?? 0)
        totalTime = Int(time.second)
        timeLbl.text = "\(timeFormatted(totalTime))"
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
       
    }
    @objc func playerDidFinishPlaying() {
        btnPlay.image(normal: "playBtn")
        print("")
    }
    @objc
    func updateTime() {
        timeLbl.text = "\(timeFormatted(totalTime))"
        if totalTime != 0 {
            totalTime -= 1
            
        }else{
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        self.totalTime = 179
      
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //   let hours: Int = totalSeconds / 3600
        return String(format: "%02d sec", minutes, seconds)
    }
    
    @objc func clickPlay() {
        
        if btnPlay.tag == 2 {
            btnPlay.image(normal: "pause")
            btnPlay.tag = 1
            startTimer()
          
            guard let url = URL.init(string: (data?.previewUrl!)!) else { return }
                   let playerItem = AVPlayerItem.init(url: url)
                   player = AVPlayer.init(playerItem: playerItem)
                   player?.play()
              
        }else {
            btnPlay.image(normal: "playBtn")
            player?.pause()
            
            btnPlay.tag = 2
        }
        
     
    }
    func stop() {
        player?.seek(to: .zero)
        player?.pause()
    }
    // MARK: - Internal Method
 
    func configure(section : Int,data : ItunesDataModel) {
        self.data = data
        switch data.kind {
        case "song":
            songLayout()
        case "feature-movie":
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.clickVideo))
         
            self.imgView.addGestureRecognizer(gesture)
              
            let time = Double(data.trackTimeMillis ?? 0)
            timeLbl.text = "\(time.minuteSecondMS)"
            moviesLayout()
            aboutLbl.text = "About the Movie"
            longDescLbl.text = "\(data.longDescription == nil ? data.collectionCensoredName! : data.longDescription!)"
        
        default:
            audiobookLayout()
        }
    }
    // MARK: - Selector

    @objc func clickPlayer() {
       
        delegate?.clickPlayer(self, previewUrl: data?.previewUrl ?? ".")
    }
    @objc func clickVideo() {
        print(data?.previewUrl,"URLL")
        delegate?.clickImage(self, previewUrl: data?.previewUrl ?? ".")
    }
}

//MARK: - Layout
extension DetailsCell {
    private func songLayout() {
        
     
     
        contentView.addSubview(musicView)
   
        musicView.addSubview(playerView)
        playerView.addSubview(btnPlay)
        playerView.addSubview(timeLbl)
        
      
        musicView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        musicView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        musicView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        musicView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        
    
        
        playerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        playerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        playerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        playerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        musicView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        btnPlay.centerXAnchor.constraint(equalTo: playerView.centerXAnchor).isActive = true
        btnPlay.centerYAnchor.constraint(equalTo: playerView.centerYAnchor).isActive = true
       
        btnPlay.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnPlay.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        timeLbl.topAnchor.constraint(equalTo: btnPlay.bottomAnchor,constant: 5).isActive = true
        timeLbl.centerXAnchor.constraint(equalTo: btnPlay.centerXAnchor).isActive = true
      
       
 
     
       
    }
    private func audiobookLayout() {
        contentView.backgroundColor = .black
        contentView.addSubview(playerView)
        playerView.addSubview(btnPlay)
        playerView.addSubview(trackNameLbl)
        contentView.addSubview(trailerLbl)
        contentView.addSubview(timeLbl)
     //   contentView.addSubview(longDescLbl)
        contentView.addSubview(aboutLbl)
        timeLbl.isHidden = true
        contentView.addSubview(webView )
        let desc = "\(data?.longDescription == nil ? data?.description : data?.longDescription)"
        print(desc)
        var htmlText = desc.replacingOccurrences(of: "&nbsp;", with: " ")
        htmlText = htmlText.replacingOccurrences(of: "&lt;", with: "<");
        htmlText = htmlText.replacingOccurrences(of: "&gt;", with: ">");
        htmlText = htmlText.replacingOccurrences(of: "&amp;", with: "&");
        for i in iTunesDatas {
           
           if data?.trackId == i.trackId{
              
               webView.loadHTMLString(i.description ?? ".", baseURL: nil)
           }
        }
        
       
         
        trailerLbl.text = "Listen"
        trailerLbl.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        trailerLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        
        playerView.topAnchor.constraint(equalTo: trailerLbl.bottomAnchor, constant: 12).isActive = true
        playerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        playerView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        playerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        playerView.layer.borderColor = .beeYellow
        playerView.layer.borderWidth = 1
        
        btnPlay.centerYAnchor.constraint(equalTo: playerView.centerYAnchor).isActive = true
        btnPlay.leadingAnchor.constraint(equalTo: playerView.leadingAnchor, constant: 10).isActive = true
        btnPlay.widthAnchor.constraint(equalToConstant: 20).isActive = true
        btnPlay.heightAnchor.constraint(equalToConstant: 20).isActive = true
        btnPlay.layer.cornerRadius = 10
        
        trackNameLbl.centerYAnchor.constraint(equalTo: btnPlay.centerYAnchor).isActive = true
        trackNameLbl.leadingAnchor.constraint(equalTo: btnPlay.trailingAnchor, constant: 10).isActive = true
        trackNameLbl.trailingAnchor.constraint(equalTo: playerView.trailingAnchor, constant: -10).isActive = true
        trackNameLbl.text = data?.artistName ?? "."
        
        
        timeLbl.topAnchor.constraint(equalTo: playerView.bottomAnchor, constant: 0).isActive = true
        
        timeLbl.leadingAnchor.constraint(equalTo: playerView.leadingAnchor, constant: 0).isActive = true
        
        timeLbl.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        aboutLbl.topAnchor.constraint(equalTo: timeLbl.bottomAnchor, constant: 12).isActive = true
        aboutLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        aboutLbl.text = "Publisher Description"
       
        webView.topAnchor.constraint(equalTo: aboutLbl.bottomAnchor, constant: 10).isActive = true
        webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        webView.widthAnchor.constraint(equalToConstant: screen_width).isActive = true
        webView.heightAnchor.constraint(equalToConstant: screen_height).isActive = true
        
        
        contentView.bottomAnchor.constraint(equalTo: webView.bottomAnchor, constant: 10).isActive = true
       

    }
    
    private func moviesLayout() {
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.clickVideo))
     
        self.playerView.addGestureRecognizer(gesture)
        
        contentView.backgroundColor = .black
        contentView.addSubview(imgView)
        contentView.addSubview(trailerLbl)
        contentView.addSubview(timeLbl)
        contentView.addSubview(longDescLbl)
        contentView.addSubview(aboutLbl)
         
       
        trailerLbl.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        trailerLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        
        imgView.topAnchor.constraint(equalTo: trailerLbl.bottomAnchor, constant: 12).isActive = true
        imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        imgView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        timeLbl.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 0).isActive = true
        
        timeLbl.leadingAnchor.constraint(equalTo: imgView.leadingAnchor, constant: 0).isActive = true
        
        timeLbl.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        aboutLbl.topAnchor.constraint(equalTo: timeLbl.bottomAnchor, constant: 12).isActive = true
        aboutLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        
        longDescLbl.topAnchor.constraint(equalTo: aboutLbl.bottomAnchor, constant: 10).isActive = true
        longDescLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        longDescLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        
        
        contentView.bottomAnchor.constraint(equalTo: longDescLbl.bottomAnchor, constant: 10).isActive = true
       

    }
  
}
extension TimeInterval {
    var hourMinuteSecondMS: String {
        String(format:"%d:%02d:%02d.%03d", hour, minute, second, millisecond)
    }
    var minuteSecondMS: String {
        String(format:"%d:%02d.%03d", minute, second, millisecond)
    }
    var hour: Int {
        Int((self/3600).truncatingRemainder(dividingBy: 3600))
    }
    var minute: Int {
        Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}
