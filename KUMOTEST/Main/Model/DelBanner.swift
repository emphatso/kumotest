//
//  DelBanner.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/7/22.
//

import Foundation
import SwiftyJSON

class DelBanner {
    var url: String?
    var img: String?
    var link: String?
    var key: Int?
    
    init(_ json: JSON) {
        url = json["url"].string
        img = json["img"].string
        link = json["link"].string
        key = json["key"].int
    }
}
