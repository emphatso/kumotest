//
//  DetailsViewController.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/5/22.
//

import AVKit
import AVFoundation
import UIKit
import WebKit

class DetailsViewController: BaseViewController {

    // MARK: - Properties
    var iTunesData : ItunesDataModel?
    var player : AVPlayer?
    private var titleStr = ""
    private var contentStr = ""
    var playerController = AVPlayerViewController()
    private var categHeight: NSLayoutConstraint!

    // MARK: - UI Component
    private let scrollContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    private var basketTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .black
        tableView.separatorStyle = .singleLine
     
        tableView.isScrollEnabled = false
        tableView.register(cellType: DetailsCell.self)
        
        return tableView
    }()
    
    // MARK: - Override
    convenience init(data: ItunesDataModel) {
        self.init()
        self.iTunesData = data
        print(data,"HAHA")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()


        layout()
    }
    override func viewDidDisappear(_ animated: Bool) {
        if let indexPath = self.basketTableView.indexPathsForVisibleRows {
            for i in indexPath {
             let cell : DetailsCell = self.basketTableView.cellForRow(at: i) as! DetailsCell
                cell.stop()
         }
       }
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        basketTableView.dataSource = self
        basketTableView.delegate = self
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow, title: iTunesData?.trackName ?? iTunesData?.collectionName)
        showBackButton()
        
        view.addSubview(scrollContentView)

        scrollContentView.addSubview(scrollView)
        scrollView.addSubview(basketTableView)
    
        
       
    }

   

    // MARK: - Selector

   
    @objc func clickFav() {
      
        
    }

    
}

// MARK: - Layout
extension DetailsViewController {
    private func layout() {
        let margins = view.safeAreaLayoutGuide
      
        scrollContentView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        scrollContentView.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        scrollContentView.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        scrollContentView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: scrollContentView.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor).isActive = true
        scrollView.heightAnchor.constraint(equalTo: scrollContentView.heightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: basketTableView.bottomAnchor, constant: 20).isActive = true
        
//        bannerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
//        bannerView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor).isActive = true
//        bannerView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor).isActive = true
//        bannerView.heightAnchor.constraint(equalToConstant: 184*screen_width/375).isActive = true
        
        basketTableView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        basketTableView.trailingAnchor.constraint(equalTo: margins.trailingAnchor ).isActive = true
        basketTableView.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        categHeight = basketTableView.heightAnchor.constraint(equalToConstant: 0)
        categHeight.isActive = true

        let height = self.basketTableView.contentSize.height
        self.categHeight.constant = height
        self.view.layoutIfNeeded()

            
            
    }
}
// MARK: - UITableViewDataSource
extension DetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         if iTunesData?.kind == "song" {
             let headerView = UIView()
             lazy var btnFav: BaseButton = {
                 let btn = BaseButton()
                 btn.image(normal: "heart2")
               
                 btn.backgroundColor = .white
                 btn.roundCorners(radius: 15, corners: .allCorners)
                 return btn
             }()
             lazy var trackNameLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = .boldSystemFont(ofSize: 18)
                label.textColor = .white
                label.textAlignment = .left
                return label
             }()
          
             lazy var trackGenreLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = .godo_M(15)
                label.textColor = .lightGray
                label.textAlignment = .left
                return label
             }()

             lazy var trackImg: UIImageView = {
                 let imageView = UIImageView()
                 imageView.translatesAutoresizingMaskIntoConstraints = false
                 imageView.contentMode = .scaleAspectFit
                 imageView.clipsToBounds = false
                 return imageView
             }()
         
             if let imageUrl = iTunesData?.artworkUrl100, let url = URL(string: imageUrl) {
     //            imageView.load(url: url)
                 print(imageUrl,"WTFFF")
                trackImg.kf.setImage(with: url)
             }
              
             headerView.backgroundColor = .black
            
             headerView.addSubview(trackImg)
             headerView.addSubview(trackNameLbl)
    
           
             headerView.addSubview(trackGenreLbl)

             
             trackNameLbl.text = "\(iTunesData!.trackName == nil ? iTunesData!.collectionName! : iTunesData!.trackName!)"
         
          
             trackGenreLbl.text = iTunesData?.primaryGenreName ?? "N/A"
         
             trackImg.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
             trackImg.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
             trackImg.centerYAnchor.constraint(equalTo: headerView.centerYAnchor, constant: 0).isActive = true
             trackImg.heightAnchor.constraint(equalToConstant:  300*screen_height/667).isActive = true
             trackImg.widthAnchor.constraint(equalToConstant: screen_width).isActive = true
            
             
             trackNameLbl.centerXAnchor.constraint(equalTo: trackImg.centerXAnchor, constant: 5).isActive = true
             trackNameLbl.topAnchor.constraint(equalTo: trackImg.bottomAnchor, constant: 10).isActive = true
             trackNameLbl.bottomAnchor.constraint(equalTo: trackGenreLbl.topAnchor, constant: -20).isActive = true
             
            trackGenreLbl.centerXAnchor.constraint(equalTo: trackNameLbl.centerXAnchor).isActive = true
             trackGenreLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
             
           
             
           
           
             
             return headerView
         }else {
             let headerView = UIView()
             lazy var btnFav: BaseButton = {
                 let btn = BaseButton()
                 btn.image(normal: "heart2")
               
                 btn.backgroundColor = .white
                 btn.roundCorners(radius: 15, corners: .allCorners)
                 return btn
             }()
             lazy var trackNameLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = .boldSystemFont(ofSize: 15)
                label.textColor = .white
                label.textAlignment = .left
                return label
             }()
             lazy var trackPriceLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = .godo_M(12)
                label.textColor = .white
                label.textAlignment = .left
                return label
             }()
             lazy var trackGenreLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.font = .godo_M(13)
                label.textColor = .lightGray
                label.textAlignment = .left
                return label
             }()
   
     
             lazy var trackImg: UIImageView = {
                 let imageView = UIImageView()
                 imageView.translatesAutoresizingMaskIntoConstraints = false
                 imageView.contentMode = .scaleAspectFit
                 imageView.clipsToBounds = false
                 return imageView
             }()
             lazy var ratingLabel: BaseButton = {
                 let btn = BaseButton()
                 btn.image(normal: "listStar")
                 btn.title("", color: .white, size: 11, alignment: .left, isBold: false)
                 btn.alignHorizontal(spacing: 4)
                 return btn
             }()
  
             if let imageUrl = iTunesData?.artworkUrl100, let url = URL(string: imageUrl) {
     //            imageView.load(url: url)
                 print(imageUrl,"WTFFF")
                trackImg.kf.setImage(with: url)
             }
              
             headerView.backgroundColor = .black
            
             headerView.addSubview(trackImg)
             headerView.addSubview(trackNameLbl)
             headerView.addSubview(trackPriceLbl)
             headerView.addSubview(ratingLabel)
             headerView.addSubview(trackGenreLbl)

             
             trackNameLbl.text = "\(iTunesData!.trackName == nil ? iTunesData!.collectionName! : iTunesData!.trackName!)"
             trackPriceLbl.text = "\(iTunesData?.trackPrice ?? iTunesData?.collectionPrice ?? 0)"
             ratingLabel.setTitle("\(iTunesData?.trackCount ?? 0)", for: .normal)
             trackGenreLbl.text = iTunesData?.primaryGenreName ?? "N/A"
         
             trackImg.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
             trackImg.centerYAnchor.constraint(equalTo: headerView.centerYAnchor, constant: 0).isActive = true
             trackImg.heightAnchor.constraint(equalToConstant: 100).isActive = true
             trackImg.widthAnchor.constraint(equalToConstant: 80).isActive = true
             
             trackNameLbl.leadingAnchor.constraint(equalTo: trackImg.trailingAnchor, constant: 10).isActive = true
             trackNameLbl.topAnchor.constraint(equalTo: trackImg.topAnchor, constant: 5).isActive = true
             trackNameLbl.bottomAnchor.constraint(equalTo: trackGenreLbl.topAnchor, constant: 0).isActive = true
             
             trackGenreLbl.leadingAnchor.constraint(equalTo: trackImg.trailingAnchor, constant: 10).isActive = true
             
             trackPriceLbl.topAnchor.constraint(equalTo: trackGenreLbl.bottomAnchor, constant: 10).isActive = true
           
             trackPriceLbl.leadingAnchor.constraint(equalTo: trackImg.trailingAnchor, constant: 10).isActive = true
             
             
           
             ratingLabel.topAnchor.constraint(equalTo: trackPriceLbl.bottomAnchor, constant: 10).isActive = true
             
             ratingLabel.leadingAnchor.constraint(equalTo: trackImg.trailingAnchor, constant: 10).isActive = true
             
             
             
             return headerView
         }
     }
  
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if iTunesData?.kind == "song" {
             return 500
         }else {
             return 150
         }
        
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        let time = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: time) {
            
            let height = self.basketTableView.contentSize.height
            self.categHeight.constant = height
        
           self.view.layoutIfNeeded()
   
        }
  
        return 1
    }
    @objc private func clickVmore(_ sender: UIButton) {
     //   let index = IndexPath(row: sender.view!.tag, section: 0)
     

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: DetailsCell.self)
     
        cell.configure(section: indexPath.row, data: iTunesData!)
        cell.delegate = self
        
        return cell
    }
  
}
extension DetailsViewController: UITableViewDelegate {
    
    
}

// MARK: - DetailsViewDelegate
extension DetailsViewController: DetailsViewDelegate {
    func clickPlayer(_ view: DetailsCell, previewUrl: String) {
        let vc = SeeMoreViewController()
        let navigate = BaseNavigationController(rootViewController: vc)
        DispatchQueue.main.async {
            self.modalShow(navigate) {
                //
            }
        }
    }
    
    
    func clickImage(_ view: DetailsCell, previewUrl: String) {
        let videoURL = URL(string: previewUrl)
      
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}


