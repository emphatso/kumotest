//
//  HomeViewController.swift
//  LetsBee
//
//  Created by WonJun Choi on 2020/04/24.
//  Copyright © 2020 WonJun Choi. All rights reserved.
//

import UIKit
import GoogleSignIn
import SwiftUI
import SwiftyJSON
import Alamofire
import Foundation
import UIKit


class HomeViewController: BaseViewController {
    // MARK: - Properties
    var offSet: CGFloat = 0
    var lineOffSet: CGFloat = 0
    private var bee = ""
    private var point = ""
    private var categHeight: NSLayoutConstraint!
    var itunesData = [AllItunesData]() {
        didSet {
            basketTableView.reloadData()
        }
    }
  
    // MARK: - UI Component
    private var basketTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = 250
   
        tableView.separatorStyle = .none
        tableView.separatorInset.right = 100
        tableView.separatorInset.left = 100
        tableView.isScrollEnabled = false
        tableView.register(cellType: ItunesCell.self)
        
        return tableView
    }()
    private let scrollContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        getItunes()
        basketTableView.dataSource = self
        basketTableView.delegate = self
      
        initialSetting()
        
        view.addSubview(scrollContentView)
        scrollContentView.addSubview(scrollView)
        scrollView.addSubview(basketTableView)
      
        layout()
    
      
        //Go to Previously Page
        if selectedData != nil {
            let time = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: time) {
            let vc = SeeMoreViewController()
         
            vc.itunesData = selectedData!
           
                self.show(vc)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow, title: "Movies")
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // getMainBanner()
 
      
        mainPopup()
      
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow)
        navigationController?.navigationBar.backgroundColor = .beeYellow
       
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .beeYellow
        
     
    }
    

    


    
    // MARK: - Network
 
  
    private func getItunes() {
        MainService().getItunesData()
        { (responseObj: ItunesData)->() in

            let responseData = responseObj.results
           
            
           
            responseData!.forEach { (item) in
                let orderHistory = AllItunesData.init(primaryGenreName: item.primaryGenreName ?? ".", results: [item])

                // START: insert All Order History data
                if !self.itunesData.contains(where: { $0.primaryGenreName == item.primaryGenreName }){
                    self.itunesData.append(orderHistory)
                  
                }else{
                    if let row = self.itunesData.firstIndex(where: {$0.primaryGenreName == item.primaryGenreName}) {
                        self.itunesData[row].results!.append(item)
                        
                       
                        if !self.itunesData[row].results!.contains(where: {$0.primaryGenreName == item.primaryGenreName}){
                           
                            self.itunesData[row].results!.append(item)
                            
                        }
                    }
                }// END: insert All Order History data
                
            }// END: get Response Data JSON
     
        }
    }
    
   
    
 
    
    private func mainPopup() {
        
        guard let start = mainPopUpCloseDatex else{
           
            return
        }
        
        let end = Date()
        
        let intervalDay = Calendar.current.dateComponents([.day], from: start, to: end).day ?? 0
        
        if intervalDay >= 1 {
         
        }
    }
    
    
    // MARK: - Selector
  
    @objc private func clickVmore(_ sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: sender.tag)
      
        let vc = SeeMoreViewController()
        let data = (itunesData[indexPath.section].results)!
        var selectedMenu = [ItunesDataModel]()
      
        selectedMenu.append(contentsOf:data)
        selectedData = selectedMenu
        vc.itunesData = data
       
            show(vc)

    }
  
}

// MARK: - LayOut
extension HomeViewController {

    private func layout() {
        let margins = view.safeAreaLayoutGuide
        
       
       
        scrollContentView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        scrollContentView.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        scrollContentView.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        scrollContentView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: scrollContentView.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor).isActive = true
        scrollView.heightAnchor.constraint(equalTo: scrollContentView.heightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: basketTableView.bottomAnchor, constant: 20).isActive = true
 
        basketTableView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        basketTableView.trailingAnchor.constraint(equalTo: margins.trailingAnchor,constant: -20).isActive = true
        basketTableView.leadingAnchor.constraint(equalTo: margins.leadingAnchor,constant: 20).isActive = true
        categHeight = basketTableView.heightAnchor.constraint(equalToConstant: basketTableView.contentSize.height)
        categHeight.isActive = true



       
        
    
    }
}

// MARK: - UITableViewDataSource

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        let time = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: time) {
            
            let height = self.basketTableView.contentSize.height
            self.categHeight.constant = height + 100
        
           self.view.layoutIfNeeded()
   
        }
       
        return itunesData.count
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         
        let headerView = UIView()
        lazy var baseTitleLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = .godo_M(16)
            label.textColor = .black
            label.textAlignment = .left
            return label
        }()
        lazy var seeMoreLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = .godo_M(10)
            label.textColor = .black
            label.text = "See more"
            label.textAlignment = .left
            return label
        }()
        lazy var underLineView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = .beeYellow
            return view
        }()
         let minusBtn: BaseButton = {
            let button = BaseButton()
             button.tag = section
           
             button.image(normal: "rightarrow")
             return button
        }()
        minusBtn.addTarget(self, action: #selector(clickVmore), for: .touchUpInside)
         minusBtn.tag = section
        headerView.frame = CGRect(x: 0, y: 0, width: 160, height: 250)
        headerView.backgroundColor = .white
        headerView.addSubview(baseTitleLabel)
        headerView.addSubview(seeMoreLbl)
        headerView.addSubview(minusBtn)
        baseTitleLabel.text = itunesData[section].primaryGenreName ?? ""
        baseTitleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 0).isActive = true
        baseTitleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 0).isActive = true
       
        seeMoreLbl.centerYAnchor.constraint(equalTo: baseTitleLabel.centerYAnchor).isActive = true
        seeMoreLbl.leadingAnchor.constraint(equalTo: baseTitleLabel.trailingAnchor, constant: 10).isActive = true
        
        minusBtn.centerYAnchor.constraint(equalTo: seeMoreLbl.centerYAnchor).isActive = true
        minusBtn.leadingAnchor.constraint(equalTo: seeMoreLbl.trailingAnchor, constant: 10).isActive = true
        minusBtn.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
        minusBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        minusBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            
           
        let wid = baseTitleLabel.sizeThatFits(baseTitleLabel.frame.size)
                
      
        headerView.addSubview(underLineView)
        headerView.bringSubviewToFront(baseTitleLabel)
        underLineView.bottomAnchor.constraint(equalTo: baseTitleLabel.bottomAnchor).isActive = true
        underLineView.leadingAnchor.constraint(equalTo: baseTitleLabel.leadingAnchor).isActive = true
        underLineView.heightAnchor.constraint(equalToConstant: 9).isActive = true
        underLineView.widthAnchor.constraint(equalToConstant: wid.width).isActive = true
       
         
 
                return headerView
            }
  
         func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 30
        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ItunesCell.self)
        cell.frame = CGRect(x: 0, y: 0, width: 160, height: 250)
        let se = itunesData[indexPath.section]
        cell.configure(menu: [se])
        cell.delegate = self
        return cell
    }
}

// MARK: - UITableViewDataSource

extension HomeViewController: UITableViewDelegate {
    
    
}

// MARK: - ItunesCellDelegates
extension HomeViewController: ItunesCellDelegates {
  
    func clickItunes(_ view: ItunesCell, compList: ItunesDataModel) {
        let vc = DetailsViewController()
        vc.iTunesData = compList
     
              show(vc)
    }
    
   
    
}
