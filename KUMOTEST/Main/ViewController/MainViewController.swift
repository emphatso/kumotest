//
//  MainViewController.swift
//  LetsBee
//
//  Created by WonJun Choi on 2020/04/24.
//  Copyright © 2020 WonJun Choi. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    // MARK: - Properties
    
    private var currentLat = ""
    private var currentLng = ""
    
    // MARK: - UI Component
    private let ciStackView: UIStackView = {
        let view = UIStackView()
        view.frame = CGRect(x: 0, y: 0, width: 94, height: 16)
        view.axis = .horizontal
        view.alignment = .leading
        view.distribution = .fill
        return view
    }()
    private let ciImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ci_new")
        imageView.frame = CGRect(x: 0, y: 0, width: 98, height: 26)
        return imageView
    }()
    private let ciContainerView: UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 98, height: 26)
        return view
    }()
    private let alertButton: BaseButton = {
        let button = BaseButton(frame: CGRect(x: 0, y: 0, width: 18, height: 20))
        return button
    }()
    private let alertImageView: UIImageView = {
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 20))
        view.image = UIImage(named: "icoHeaderNoti")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let badgeView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .beeRed
        view.layer.cornerRadius = 7.5
        view.layer.masksToBounds = true
        view.isHidden = true
        return view
    }()
    private let badgeCount: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(9), color: .white, alignment: .center)
        return label
    }()
    
    //MARK: - child
    private let musicVC: MusicViewController = {
        let controller = MusicViewController()
        return controller
    }()
    private let favoritesVC: FavoritesViewController = {
        let controller = FavoritesViewController()
        return controller
    }()
    private let moviesVC: HomeViewController = {
        let controller = HomeViewController()
        return controller
    }()
    private let searhVC: SearchRecentViewController = {
        let controller = SearchRecentViewController()
        return controller
    }()
    
 
    //MARK: - override
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
        updateNavTitle(title: "iTunes")

        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
     
    }
    
    override func viewDidLayoutSubviews() {
              
        if screen_height > 667 {
            tabBar.frame.size.height = 90
            tabBar.frame.origin.y = view.frame.height - 90
        } else {
            tabBar.frame.size.height = 70
            tabBar.frame.origin.y = view.frame.height - 70
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        }

    }
    
    //MARK: - Private Method
    private func initialSetting() {
        makeTabbar()
        swipeGesture()
        //Setup TabBar
        
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().tintColor = .white
        let tbItemProxy = UITabBarItem.appearance()
        let selectedColor   = UIColor.beeBrown
        let unselectedColor = UIColor.rgb(170, 162, 157)
        
        tbItemProxy.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor, NSAttributedString.Key.font: UIFont(name: "GodoM", size: 9.5) ?? UIFont.systemFont(ofSize: 9.5)], for: .normal)
        tbItemProxy.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor, NSAttributedString.Key.font: UIFont(name: "GodoM", size: 9.5) ?? UIFont.systemFont(ofSize: 9.5)], for: .selected)
        tbItemProxy.setBadgeTextAttributes([NSAttributedString.Key.font: UIFont(name: "GodoM", size: 7) ?? UIFont.systemFont(ofSize: 7)], for: .normal)
        tbItemProxy.badgeColor = .red

        navigationItem.hidesBackButton = true
        navigationItem.titleView = ciContainerView
        
   
        
     
        
    }
    
    private func makeTabbar() {
        let tabFirst = UITabBarItem(title: MainTab.music.title, image: MainTab.music.image.withRenderingMode(.alwaysOriginal), selectedImage: MainTab.music.imageSelect.withRenderingMode(.alwaysOriginal))
        
        let tabSecond = UITabBarItem(title: MainTab.movies.title, image: MainTab.movies.image.withRenderingMode(.alwaysOriginal), selectedImage: MainTab.movies.imageSelect.withRenderingMode(.alwaysOriginal))
        
        let tabThird = UITabBarItem(title: MainTab.favorites.title, image: MainTab.favorites.image.withRenderingMode(.alwaysOriginal), selectedImage: MainTab.favorites.imageSelect.withRenderingMode(.alwaysOriginal))
        
        let tabFourth = UITabBarItem(title: MainTab.search.title, image: MainTab.search.image.withRenderingMode(.alwaysOriginal), selectedImage: MainTab.search.imageSelect.withRenderingMode(.alwaysOriginal))

        
        tabFirst.tag = 0
        tabSecond.tag = 1
        tabThird.tag = 2
        tabFourth.tag = 3
        
        musicVC.tabBarItem = tabFirst
        moviesVC.tabBarItem = tabSecond
        favoritesVC.tabBarItem = tabThird
        searhVC.tabBarItem = tabFourth
        
        self.viewControllers = [musicVC, moviesVC, favoritesVC, searhVC]
        
    }
    
    private func swipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)

    }
    
   
    
    private func setNavTitle(title: String) {
        
        let containerView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        let navTitleLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = title
            label.textAlignment = .center
            label.font = .godo_B(14)
            label.textColor = .black
            label.numberOfLines = 2
            return label
        }()
        containerView.addSubview(navTitleLabel)
        containerView.widthAnchor.constraint(equalToConstant: screen_width).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: navigationController?.navigationBar.frame.height ?? 48).isActive = true
        
        
        
        navTitleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        navTitleLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
      
        
      
        
        navigationItem.titleView = containerView
        
    }
    
    func setIndex(index: Int) {
        
        let title = BaseLabel()
        title.configure(font: .godo_B(16), color: .black)

        if index == 0 {
            title.text = "Music"
            navigationItem.titleView = title
            navigationItem.rightBarButtonItem = nil
        } else if index == 1 {
            title.text = "Movies"
            navigationItem.titleView = title
            navigationItem.rightBarButtonItem = nil
        } else if index == 2 {
            title.text = "Favorites"
            navigationItem.titleView = title
            navigationItem.rightBarButtonItem = nil
        } else {
            title.text = "Search"
            navigationItem.titleView = title
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    
    // MARK: - Internal Method
    func currentLocation() {
      
    }
    
    func updateNavTitle(title: String) {
        setNavTitle(title: title)
    }
    
    func updateNavTitle(latitude: Double, longitude: Double) {
      
    }
    
    // MARK: - Network
  

    // MARK: - Selector
    @objc func handleSwipeGesture(_ gesture: UISwipeGestureRecognizer) {
        
        if gesture.direction == .left {
            if (self.selectedIndex) < 3 { // 슬라이드할 탭바 갯수 지정 (전체 탭바 갯수 - 1)
                animateToTab(toIndex: self.selectedIndex+1)
                setIndex(index: self.selectedIndex+1)
            }
        } else if gesture.direction == .right {
            if (self.selectedIndex) > 0 {
                animateToTab(toIndex: self.selectedIndex-1)
                setIndex(index: self.selectedIndex-1)
            }
        }
    }
    
    @objc func didGetPush() {
      
        
    }
    
    @objc func didClickPush() {
     
    }
    
    @objc func didClickTitleButton() {
        print("didClickTitleButton")
        
      
    }
    
   
}

//MARK: - UITabBarControllerDelegate
extension MainViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let tabViewControllers = tabBarController.viewControllers, let toIndex = tabViewControllers.firstIndex(of: viewController) else {
            return false
        }
        
        animateToTab(toIndex: toIndex)
        setIndex(index: toIndex)

        return true
    }
        
    func animateToTab(toIndex: Int) {
        guard let tabViewControllers = viewControllers,
            let selectedVC = selectedViewController else { return }
        
        guard let fromView = selectedVC.view,
            let toView = tabViewControllers[toIndex].view,
            let fromIndex = tabViewControllers.firstIndex(of: selectedVC),
            fromIndex != toIndex else { return }
        
        
        // Add the toView to the tab bar view
        fromView.superview?.addSubview(toView)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseOut,
                       animations: {
                        // Slide the views by -offset
                        fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                        toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
                        
        }, completion: { finished in
            // Remove the old view from the tabbar view.
            fromView.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard let tabViewControllers = tabBarController.viewControllers, let idx = tabViewControllers.firstIndex(of: viewController) else {return}
        
        setIndex(index: idx)

    }
    

}

