//
//  SeeMoreViewController.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import UIKit
import GoogleSignIn
import SwiftUI



class SeeMoreViewController: BaseViewController {
    // MARK: - Properties
    var offSet: CGFloat = 0
    var lineOffSet: CGFloat = 0
    private var bee = ""
    private var point = ""

    private var categHeight: NSLayoutConstraint!
    var itunesData = [ItunesDataModel]() {
        didSet {
            basketTableView.reloadData()
        }
    }
   
    // MARK: - UI Component
    private var basketTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .black
        tableView.separatorStyle = .singleLine
        tableView.separatorInset.right = 100
        tableView.separatorInset.left = 100
        tableView.isScrollEnabled = false
        tableView.register(cellType: SeemoreCell.self)
        
        return tableView
    }()
    private let scrollContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()

   
    override func viewDidDisappear(_ animated: Bool) {
              
                UserDefaults.standard.removeObject(forKey: selectedDatas)
              
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basketTableView.dataSource = self
        basketTableView.delegate = self
      
        initialSetting()
        view.addSubview(scrollContentView)

        scrollContentView.addSubview(scrollView)
       
        scrollView.addSubview(basketTableView)
      
        layout()
        
     
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showBackButton()
   
    }
    
    override func viewDidAppear(_ animated: Bool) {

        mainPopup()
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow, title: itunesData[0].primaryGenreName ?? ".")

     
        navigationController?.navigationBar.backgroundColor = .beeYellow
       
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .beeYellow
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow)
        
 //       Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
      
    }
    

    


    
    // MARK: - Network
 
  
    private func mainPopup() {
        
        guard let start = mainPopUpCloseDatex else{
           
            return
        }
        
        let end = Date()
        
        let intervalDay = Calendar.current.dateComponents([.day], from: start, to: end).day ?? 0
        
        if intervalDay >= 1 {
         
        }
    }
    
    
    // MARK: - Selector
    @objc func swipeDown() {
        self.dismiss(animated: true)
    }

}




// MARK: - LayOut
extension SeeMoreViewController {

    private func layout() {
        let margins = view.safeAreaLayoutGuide
        
       
       
        scrollContentView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        scrollContentView.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        scrollContentView.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        scrollContentView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: scrollContentView.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor).isActive = true
        scrollView.heightAnchor.constraint(equalTo: scrollContentView.heightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: basketTableView.bottomAnchor, constant: 20).isActive = true

        basketTableView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        basketTableView.trailingAnchor.constraint(equalTo: margins.trailingAnchor,constant: -20).isActive = true
        basketTableView.leadingAnchor.constraint(equalTo: margins.leadingAnchor,constant: 20).isActive = true
        categHeight = basketTableView.heightAnchor.constraint(equalToConstant: basketTableView.contentSize.height)
        categHeight.isActive = true



       
        
    
    }
}

// MARK: - UITableViewDataSource
extension SeeMoreViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let headerView = UIView()
         lazy var baseTitleLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = .godo_M(16)
            label.textColor = .black
            label.textAlignment = .left
            return label
         }()
 
        lazy var underLineView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = .beeYellow
            return view
        }()
  
       
        headerView.frame = CGRect(x: 0, y: 0, width: 160, height: 250)
        headerView.backgroundColor = .white
        headerView.addSubview(baseTitleLabel)
        baseTitleLabel.text = "Result"
        baseTitleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 0).isActive = true
        baseTitleLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 0).isActive = true
       
        let wid = baseTitleLabel.sizeThatFits(baseTitleLabel.frame.size)
      
        headerView.addSubview(underLineView)
        headerView.bringSubviewToFront(baseTitleLabel)
        underLineView.bottomAnchor.constraint(equalTo: baseTitleLabel.bottomAnchor).isActive = true
        underLineView.leadingAnchor.constraint(equalTo: baseTitleLabel.leadingAnchor).isActive = true
        underLineView.heightAnchor.constraint(equalToConstant: 9).isActive = true
        underLineView.widthAnchor.constraint(equalToConstant: wid.width).isActive = true
  
         
         return headerView

     }
  
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        let time = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: time) {
            
            let height = self.basketTableView.contentSize.height
            self.categHeight.constant = height
        
           self.view.layoutIfNeeded()
   
        }
  
        return itunesData.count
    }
    @objc private func clickVmore(_ sender: UIButton) {
     //   let index = IndexPath(row: sender.view!.tag, section: 0)
     

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("indexPath: \(indexPath)")
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SeemoreCell.self)
        cell.frame = CGRect(x: 0, y: 0, width: 160, height: 250)
        let se = itunesData[indexPath.row]
       
        cell.configure(se)
       // cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let vc = DetailsViewController()
        vc.iTunesData = itunesData[indexPath.row]
     
              show(vc)
    }
}
extension SeeMoreViewController: UITableViewDelegate {
    
  
}


