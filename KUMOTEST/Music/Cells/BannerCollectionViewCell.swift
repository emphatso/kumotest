//
//  BannerCollectionViewCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//



import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    // MARK: - UI Component
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    // MARK: - Override
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetting()
        contentView.addSubview(imageView)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Internal Method
    
    func configure(_ banner: ItunesDataModel) {
        if let imageUrl = banner.artworkUrl100, let url = URL(string: imageUrl) {
            self.imageView.kf.setImage(with: url)
            
//            imageView.load(url: url)
        }
//        guard let img = banner.img else{return}
//        imageView.image = UIImage(named: img)
        
    }
    

    
    // MARK: - Private Method
    
    private func initialSetting() {
        contentView.layer.shadowColor = .rgb(0, 0.1)
        contentView.layer.shadowOffset = CGSize(width: 0, height: 1)
        contentView.layer.shadowOpacity = 1.0
        contentView.layer.shadowRadius = 0.5
        contentView.layer.masksToBounds = false
        contentView.clipsToBounds = true
    }
    
}

//MARK: - Layout
extension BannerCollectionViewCell {
    
    private func layout() {
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
}
class BannerCell : UICollectionViewCell {
    
    
}
