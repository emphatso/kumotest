//
//  HomeCategoryCollectionViewCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//

import UIKit

class HomeCategoryCollectionViewCell: UICollectionViewCell {
    // MARK: - UI Component
    var sectionz : [Int] = []
     let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = false
        
        return imageView
    }()

    private let CategoryView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .beeYellow
        view.layer.cornerRadius = 30*screen_height/667
        return view
    }()
     let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(15), color: .black, alignment: .center)
        return label
    }()
  
    // MARK: - Override
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetting()

       
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Internal Method
    
    func configure(_ category: ItunesDataModel,section : Int) {

          

           
      
        if let imageUrl = category.artworkUrl100, let url = URL(string: imageUrl) {
            self.imageView.kf.setImage(with: url)
  
        }
       
 
     
    }
    
    // MARK: - Private Method
    
    private func initialSetting() {
        contentView.addSubview(imageView)
       
        layout()
    }
}

//MARK: - Layout
extension HomeCategoryCollectionViewCell {
    private func layout2() {

        CategoryView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        CategoryView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 0).isActive = true
        CategoryView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: 0).isActive = true
        CategoryView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        imageView.topAnchor.constraint(equalTo: CategoryView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: CategoryView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: CategoryView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: CategoryView.bottomAnchor).isActive = true
            
    
    }
    private func layout() {
        
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
      
     
  
    }
}

extension UIView {

func roundCorners(radius: CGFloat = 20, corners: UIRectCorner = .allCorners) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        if #available(iOS 11.0, *) {
            var arr: CACornerMask = []
            
            let allCorners: [UIRectCorner] = [.topLeft, .topRight, .bottomLeft, .bottomRight, .allCorners]
            
            for corn in allCorners {
                if(corners.contains(corn)){
                    switch corn {
                    case .topLeft:
                        arr.insert(.layerMinXMinYCorner)
                    case .topRight:
                        arr.insert(.layerMaxXMinYCorner)
                    case .bottomLeft:
                        arr.insert(.layerMinXMaxYCorner)
                    case .bottomRight:
                        arr.insert(.layerMaxXMaxYCorner)
                    case .allCorners:
                        arr.insert(.layerMinXMinYCorner)
                        arr.insert(.layerMaxXMinYCorner)
                        arr.insert(.layerMinXMaxYCorner)
                        arr.insert(.layerMaxXMaxYCorner)
                    default: break
                    }
                }
            }
            self.layer.maskedCorners = arr
        } else {
            self.roundCornersBezierPath(corners: corners, radius: radius)
        }
    }
    
    private func roundCornersBezierPath(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}
