//
//  ItunesCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//

import UIKit

protocol ItunesCellDelegates: NSObjectProtocol {
  
    func clickItunes(_ view: ItunesCell, compList: ItunesDataModel)
}

class ItunesCell: UITableViewCell {
    
    var itunesData = [AllItunesData]() {
        didSet {
            collectionView.reloadData()
            
        }
    }
    private var vSpinner: UIView?
   
    weak var delegate: ItunesCellDelegates?
    private let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 140*screen_width/375, height: 250*screen_height/667)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
      
        
        collectionView.register(cellType: FavoritesCollectionViewCell.self)
        return collectionView
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
      
        contentView.addSubview(collectionView)
    
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
    }
  
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        contentView.backgroundColor = .white
        selectionStyle = .none
        
        
    }
    
    
    
    // MARK: - Internal Method
    func configure(menu: [AllItunesData]) {
        itunesData = menu
      
        collectionView.reloadData()
    }
 
}
//MARK: - CollectionViewDelegate
extension ItunesCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
        return itunesData[section].results!.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: FavoritesCollectionViewCell.self)
        
        cell.configure(itunesData[indexPath.section].results![indexPath.item])
 
      
        return cell
     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            return CGSize(width: 165*screen_width/375, height: 200*screen_height/667)
     
            
       
    }
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai: UIActivityIndicatorView?
        
        if #available(iOS 13.0, *) {
            ai = UIActivityIndicatorView.init(style: .large)
        } else {
            ai = UIActivityIndicatorView.init(style: .gray)
        }
        guard let indicator = ai else {return}
        indicator.startAnimating()
        indicator.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(indicator)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    @objc private func didClickBasket(_ sender:AnyObject){
  
   
    
    }
    
   
}

//MARK: - CollectionViewDelegate
extension ItunesCell: UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 
        let del = itunesData[indexPath.section].results![indexPath.item]
        
        delegate?.clickItunes(self, compList: del)
    }
    

}
