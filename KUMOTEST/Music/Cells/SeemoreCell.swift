//
//  SearchResultTableViewCell.swift
//  LetsBee
//
//  Created by WonJun Choi on 2020/07/13.
//  Copyright © 2020 WonJun Choi. All rights reserved.
//

import UIKit

class SeemoreCell: UITableViewCell {

    // MARK: - Properties
    
    // MARK: - UI Component
    private let imgView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = false
        return imageView
    }()

    private let compNameLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(14), color: .white, alignment: .left)
        label.numberOfLines = 0
        return label
    }()
    private let ratingLabel: BaseButton = {
        let btn = BaseButton()
        btn.image(normal: "listStar")
        btn.title("", color: .white, size: 11, alignment: .left, isBold: false)
        btn.alignHorizontal(spacing: 4)
        return btn
    }()
    private let reviewLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(11), color: .rgb(129), alignment: .left)
        return label
    }()
    private let exLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(11), color: .rgb(129), alignment: .left)
        return label
    }()

    
    // MARK: - Override
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .black
        initialSetting()
        contentView.addSubview(imgView)
        contentView.addSubview(compNameLabel)
        contentView.addSubview(ratingLabel)
        contentView.addSubview(reviewLabel)
        contentView.addSubview(exLabel)
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Private Method
    
    private func initialSetting() {
        
    }
    
    // MARK: - Internal Method
    
    func configure(_ list: ItunesDataModel) {
        if let img = list.artworkUrl100{
           
               
                if let url = URL(string: img) {
                    imgView.load(url: url)
                }
            }
       

        compNameLabel.text = list.trackName
        ratingLabel.setTitle("\(list.trackCount ?? 0)", for: .normal)
        reviewLabel.text = list.releaseDate ?? "."
        exLabel.text = list.longDescription?.trimHTMLTags()
        
    }
}

//MARK: - Layout
extension SeemoreCell {
    
    private func layout() {
        contentView.bottomAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 12).isActive = true
        
        imgView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12).isActive = true
        imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        imgView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        compNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12).isActive = true
        compNameLabel.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 10).isActive = true
        compNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        
        ratingLabel.topAnchor.constraint(equalTo: compNameLabel.bottomAnchor, constant: 8).isActive = true
        ratingLabel.leadingAnchor.constraint(equalTo: compNameLabel.leadingAnchor).isActive = true
        
        reviewLabel.topAnchor.constraint(equalTo: ratingLabel.topAnchor).isActive = true
        reviewLabel.leadingAnchor.constraint(equalTo: ratingLabel.trailingAnchor, constant: 10).isActive = true
        
        exLabel.topAnchor.constraint(equalTo: reviewLabel.bottomAnchor, constant: 8).isActive = true
        exLabel.leadingAnchor.constraint(equalTo: compNameLabel.leadingAnchor).isActive = true
        exLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true

    }
}
