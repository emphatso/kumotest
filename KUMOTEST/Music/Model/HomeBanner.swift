//
//  HomeBanner.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import SwiftyJSON

class HomeBanner {
    var url: String?
    var img: String?
    var link: String?
    var key: Int?
    
    init(_ json: JSON) {
        url = json["url"].string
        img = json["img"].string
        link = json["link"].string
        key = json["key"].int
    }
}

class HomeLineBanner {
    var url: String?
    var img: String?
    var link: String?
    var key: Int?
    
    init(_ json: JSON) {
        url = json["url"].string
        img = json["img"].string
        link = json["link"].string
        key = json["key"].int
    }
    
}
