//
//  HomeList.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import SwiftyJSON
import Alamofire
import Foundation
import UIKit

struct ItunesData : Decodable {
    var resultCount: Int?
    var results : [ItunesDataModel]?
    var _results: [ItunesDataModels]?
  
}


class AllItunesData : Decodable{
    var primaryGenreName: String?
    var results: [ItunesDataModel]?
    
    
    init(primaryGenreName: String, results: [ItunesDataModel]) {
        self.primaryGenreName = primaryGenreName
        self.results = results
    }
}


class ItunesDataModel : NSObject, Decodable,Encodable ,NSCoding{
    func encode(with acoder: NSCoder) {
        acoder.encode(artworkUrl100, forKey: "artworkUrl100")
        acoder.encode(trackId, forKey: "trackId")
        acoder.encode(trackName, forKey: "trackName")
        acoder.encode(trackPrice, forKey: "trackPrice")
        acoder.encode(primaryGenreName, forKey: "primaryGenreName")
        acoder.encode(trackCount, forKey: "trackCount")
        acoder.encode(longDescription, forKey: "longDescription")
        acoder.encode(artistName, forKey: "artistName")
        acoder.encode(trackCount, forKey: "trackCount")
        acoder.encode(kind, forKey: "kind")
        acoder.encode(wrapperType, forKey: "wrapperType")
        acoder.encode(previewUrl, forKey: "previewUrl")
        acoder.encode(collectionName, forKey: "collectionName")
        acoder.encode(collectionPrice, forKey: "collectionPrice")
    }
    
    required init?(coder aDecoder: NSCoder) {
        artworkUrl100 = aDecoder.decodeObject(forKey: "artworkUrl100") as? String
        trackId = aDecoder.decodeObject(forKey: "trackId") as? Int
        trackName = aDecoder.decodeObject(forKey: "trackName") as? String
        trackPrice = aDecoder.decodeObject(forKey: "trackPrice") as? Double
        primaryGenreName = aDecoder.decodeObject(forKey: "primaryGenreName") as? String
        trackCount = aDecoder.decodeObject(forKey: "trackCount") as? Int
        longDescription = aDecoder.decodeObject(forKey: "longDescription") as? String
        artistName = aDecoder.decodeObject(forKey: "artistName")  as? String
        trackCount = aDecoder.decodeObject(forKey: "trackCount") as? Int
        kind = aDecoder.decodeObject(forKey: "kind") as? String
        wrapperType = aDecoder.decodeObject(forKey: "wrapperType") as? String
        previewUrl = aDecoder.decodeObject(forKey: "previewUrl") as? String
        collectionName = aDecoder.decodeObject(forKey: "collectionName") as? String
        collectionPrice = aDecoder.decodeObject(forKey: "collectionPrice") as? Double
    }
    
    
    var wrapperType: String?
    var kind: String?
    var collectionId: Int?
    var trackId: Int?
    var artistName: String?
    var collectionName: String?
    var trackName: String?
    var collectionCensoredName: String?
    var trackCensoredName: String?
    var collectionArtistId: Int?
    
    var collectionArtistViewUrl: String?
    var collectionViewUrl: String?
    var trackViewUrl: String?
    var previewUrl: String?
    var artworkUrl30: String?
    var artworkUrl60: String?
    var artworkUrl100: String?
    var collectionPrice: Double?
    var trackPrice: Double?
    var trackRentalPrice: Double?
    var collectionHdPrice: Double?
    var trackHdPrice: Double?
    var trackHdRentalPrice: Double?
    var releaseDate: String?
    var collectionExplicitness: String?
   
    var trackExplicitness: String?
    var discCount: Int?
    var discNumber: Int?
    var trackCount: Int?
    var trackNumber: Int?
    var trackTimeMillis: Int?
    var country: String?
    var currency: String?
    var primaryGenreName: String?
    var contentAdvisoryRating: String?
    var longDescription: String?
   
    var hasITunesExtras: Bool?
    
   
}

struct ItunesDataModels : Decodable,Encodable {
   
    init(_ json: JSON) {
        description = json["description"].string
        artworkUrl100 = json["artworkUrl100"].string
        trackId = json["trackId"].int
        trackName = json["trackName"].string
        trackPrice = json["trackPrice"].double
        primaryGenreName = json["primaryGenreName"].string
        trackCount = json["trackCount"].int
        longDescription = json["longDescription"].string
        artistName = json["artistName"].string
        trackCount = json["trackCount"].int
        kind = json["kind"].string
        wrapperType = json["wrapperType"].string
    }
    
    var wrapperType: String?
    var kind: String?
    var collectionId: Int?
    var trackId: Int?
    var artistName: String?
    var collectionName: String?
    var trackName: String?
    var collectionCensoredName: String?
    var trackCensoredName: String?
    var collectionArtistId: Int?
    
    var collectionArtistViewUrl: String?
    var collectionViewUrl: String?
    var trackViewUrl: String?
    var previewUrl: String?
    var artworkUrl30: String?
    var artworkUrl60: String?
    var artworkUrl100: String?
    var collectionPrice: Double?
    var trackPrice: Double?
    var trackRentalPrice: Double?
    var collectionHdPrice: Double?
    var trackHdPrice: Double?
    var trackHdRentalPrice: Double?
    var releaseDate: String?
    var collectionExplicitness: String?
   
    var trackExplicitness: String?
    var discCount: Int?
    var discNumber: Int?
    var trackCount: Int?
    var trackNumber: Int?
    var trackTimeMillis: Int?
    var country: String?
    var currency: String?
    var primaryGenreName: String?
    var contentAdvisoryRating: String?
    var longDescription: String?
    var description: String?
   
    var hasITunesExtras: Bool?
    
   
}



