//
//  BannerView.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import UIKit
import MSPeekCollectionViewDelegateImplementation

protocol BannerViewDelegate: NSObjectProtocol {
    func bannerViewDidSelectBanner(_ view: BannerView, data: ItunesDataModel)
}
class BannerView: BaseView {

    // MARK: - Properties
    
    weak var delegate: BannerViewDelegate?
    private var peekImplementation: MSCollectionViewPeekingBehavior!
    var banners = [ItunesDataModel]()

    private var pagingWidth: CGFloat!
    private var isMain = false
    
    // MARK: - UI Component
    let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 8
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: screen_width, height: screen_width*184/375)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.register(cellType: BannerCollectionViewCell.self)
        return collectionView
    }()
    private var pagingControl: CustomPageControl = {
        let pagingControl = CustomPageControl()
        pagingControl.translatesAutoresizingMaskIntoConstraints = false
        return pagingControl
    }()
    
    // MARK: - Override
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetting()
        addSubview(collectionView)
        addSubview(pagingControl)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Private Method
    
    private func initialSetting() {
        collectionView.delegate = self
        collectionView.dataSource = self
        reloadDelegate()
        
        pagingControl.currentPage = 0
        pagingControl.isUserInteractionEnabled = false
    }
    
    // MARK: - Internal Method
    
    func mainConfigure(_ banners: [ItunesDataModel]) {
        isMain = true
        self.banners = banners
        collectionView.reloadData()
        pagingControl.numberOfPages = banners.count
        print("banners count: \(banners.count)")

    }
    
  
    
    func reloadDelegate() {
        peekImplementation = MSCollectionViewPeekingBehavior(cellSpacing: 0, cellPeekWidth: 0, maximumItemsToScroll: 5, numberOfItemsToShow: 1, scrollDirection: .horizontal)
        collectionView.configureForPeekingBehavior(behavior: peekImplementation)
        collectionView.reloadData()
    }
    
    

}

//MARK: - LayOut
extension BannerView {
    
    private func layout() {
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        pagingControl.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        pagingControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        pagingControl.widthAnchor.constraint(equalToConstant: 200).isActive = true
    }
}

//MARK: - CollectionViewDataSource
extension BannerView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
            return banners.count
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: BannerCollectionViewCell.self)
        
            let banner = banners[indexPath.item]
            cell.configure(banner)
        
            return cell
    

    }
   
    
}

//MARK: - CollectionViewDelegate

extension BannerView: UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        peekImplementation.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isMain {
            guard !(floor(scrollView.contentOffset.x / scrollView.frame.maxX).isNaN || floor(scrollView.contentOffset.x / scrollView.frame.maxX).isInfinite) else {
                return  // or do some error handling
            }
            self.pagingControl.currentPage = Int()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.bannerViewDidSelectBanner(self,data: banners[indexPath.row])
        print("Banners clicked, isMain: \(isMain), banner_idx: \(indexPath.item)")
    }

}
