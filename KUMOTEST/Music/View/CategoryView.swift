//
//  CategoryView.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import UIKit

protocol CategoryViewDelegate: NSObjectProtocol {
    func categoryViewDidSelect(_ view: CategoryView, category: ItunesDataModel)
}

class CategoryView: BaseView {
    
    // MARK: - Properties
    
    weak var delegate: CategoryViewDelegate?
    private var category = [ItunesDataModel]()
  
    
    // MARK: - UI Component
     let collectionViews: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        flowLayout.itemSize = CGSize(width: 120, height: 130)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = false
        collectionView.register(cellType: HomeCategoryCollectionViewCell.self)
        
        
        return collectionView
    }()
    
    // MARK: - Override
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetting()
        addSubview(collectionViews)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
 
    // MARK: - Private Method
    
    private func initialSetting() {
        collectionViews.delegate = self
        collectionViews.dataSource = self
        reloadDelegate()

    }
    
    // MARK: - Internal Method
    
    func configure(_ category: [ItunesDataModel]) {
      
        collectionViews.reloadData()
        self.category = category
      
       
    }
    
    func reloadDelegate() {
      collectionViews.reloadData()
    }
    
    
}
//MARK: - LayOut
extension CategoryView {
    
    private func layout() {
        collectionViews.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        collectionViews.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        collectionViews.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        collectionViews.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
    }
}

//MARK: - CollectionViewDataSource
extension CategoryView: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
            return 1
        }


        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            collectionViews.reloadData()
            print(category.count,"WEWEASDW")
          
            return category.count
         
            
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            return CGSize(width: 165*screen_width/375, height: 120*screen_height/667)
      
            
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: HomeCategoryCollectionViewCell.self)
       
       print("WTFFFARE")
        cell.configure(category[indexPath.row], section: indexPath.section )
       
        
      
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return   CGSize(width: 50, height: 10)
    }
 
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

//MARK: - CollectionViewDelegate
extension CategoryView: UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cat = category[indexPath.item]
        delegate?.categoryViewDidSelect(self, category: cat)
    }

}
