//
//  HomeListView.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/4/22.
//


import UIKit

protocol HomeListViewDelegate: NSObjectProtocol {
    func HomeListViewDidSelect(_ view: HomeListView, compList: ItunesDataModel)
}


class HomeListView: BaseView {
    

    // MARK: - Properties
    
    weak var delegate: HomeListViewDelegate?
    private var list: [ItunesDataModel]?
    
    // MARK: - UI Component
    private let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 165*screen_width/375, height: 300)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = false
        collectionView.register(cellType: HomeListCollectionViewCell.self)
        return collectionView
    }()
    // MARK: - Override
    override init(frame: CGRect) {
       super.init(frame: frame)
       
       initialSetting()
       addSubview(collectionView)
       layout()
    }

    required init?(coder: NSCoder) {
       fatalError()
    }

    // MARK: - Private Method

    private func initialSetting() {
       collectionView.delegate = self
       collectionView.dataSource = self
       reloadDelegate()

    }

    // MARK: - Internal Method

    func configure(_ list: [ItunesDataModel]) {
        self.list = list
        showTitleLabel("Rated PG", top: 20, leading: 20, underLine: true)
        
        collectionView.reloadData()
        
       
    }
    

    func reloadDelegate() {
       collectionView.reloadData()
    }
}

//MARK: - LayOut
extension HomeListView {
    
    private func layout() {
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}

//MARK: - CollectionViewDataSource
extension HomeListView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let compList = list else{return 0}
        
        return compList.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            return CGSize(width: 165*screen_width/375, height: 200*screen_height/667)
     
            
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: HomeListCollectionViewCell.self)
        guard let compList = list else{return cell}
        
        let homlist = compList[indexPath.item]
        
        
        cell.configure(homlist)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
}

//MARK: - CollectionViewDelegate
extension HomeListView: UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//      ®
    }

}
