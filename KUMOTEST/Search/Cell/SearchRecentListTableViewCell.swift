//
//  SearchRecentListTableViewCell.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/7/22.
//


import UIKit

class SearchRecentListTableViewCell: UITableViewCell {
    
    // MARK: - UI Component
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.configure(font: .godo_M(14), color: .black)
        return label
    }()
    

    // MARK: - Override
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initialSetting()
        contentView.addSubview(titleLabel)
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        contentView.backgroundColor = .white
        selectionStyle = .none
    }
    
    // MARK: - Internal Method
    func configure(title: String) {
        titleLabel.text = title
    }
    
}


// MARK: - Layout
extension SearchRecentListTableViewCell {
    private func layout() {
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
}
