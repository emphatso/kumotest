//
//  SearchViewController.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/7/22.
//


import UIKit

class SearchRecentViewController: BaseViewController {


    // MARK: - Properties
    private var recentSeachWord: [String]?
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        formatter.locale = .init(identifier: "ko-KR")
        return formatter
    }()
    private var refreshControl = UIRefreshControl()
    private let searchBar: UISearchBar = {
        let search = UISearchBar()
        search.translatesAutoresizingMaskIntoConstraints = false
        search.barTintColor = .white
        search.backgroundColor = .white
        search.layer.borderColor = .rgb(226)
        search.layer.borderWidth = 1
        search.setImage(UIImage(named: "icoSearch"), for: .search, state: .normal)
        search.placeholder = "Search Keyword"
        
        let textFeild = search.value(forKey: "searchField") as? UITextField
        textFeild?.font = UIFont.godo_M(15)
        textFeild?.textColor = .black
        textFeild?.backgroundColor = .white
        
        let placeholder = textFeild?.value(forKey: "placeholderLabel") as? UILabel
        placeholder?.font = UIFont.godo_M(15)
        placeholder?.textColor = .rgb(185)
        
        return search
    }()
    private var firstDay = ""
    private var secondDay = ""
    
    // MARK: - UI Component
 

    private let searchTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.placeholder = "Search iTunes"
        return textField
    }()

    



    private let recentTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        tableView.register(cellType: SearchRecentListTableViewCell.self)
        let footer = UIView()
        footer.backgroundColor = .clear
        tableView.tableFooterView = footer
        return tableView
    }()
    private let searchButton: BaseButton = {
        let btn = BaseButton()
        btn.round(0, color: .beeYellow, selectedColor: .beeYellow, width: 0, backgroud: .beeYellow)
        btn.title("Recent Searches", color: .black, size: 15, alignment: .center, isBold: false)
        return btn
    }()
    
    // MARK: - Override
    init() {
        super.init(nibName: nil, bundle: nil)
       
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: selectedViewController)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        recentTableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
      
   
        view.addSubview(recentTableView)
        view.addSubview(searchBar)
        layout()
        
        viewControllerVisited = "Search"
      
    }
    
    // MARK: - Private Method
    private func initialSetting() {
        showBackButton()
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow, title: "Search iTUnes")
        
        searchTextField.delegate = self
        recentTableView.delegate = self
        recentTableView.dataSource = self
        
        recentSeachWord = recentSearchWordx
        

        searchBar.delegate = self
        
        
        
      //  searchBar.text = searchText == " " ? nil:searchText
        

        
     
    }
    
    // MARK: - Internal Method

    
    
    // MARK: - Network

    
    // MARK: - Selector
    @objc private func didClickDeleteCellButton(_ sender: BaseButton) {
        let idx = sender.tag
        guard var delRecent = recentSeachWord else{return}
        delRecent.remove(at: idx)
        recentSearchWordx = delRecent
        recentSeachWord = delRecent
        recentTableView.reloadData()
        
    }
    
 

    

    
}

// MARK: - Layout
extension SearchRecentViewController {
    private func layout() {
        let margins = view.safeAreaLayoutGuide
        
        searchBar.topAnchor.constraint(equalTo: margins.topAnchor, constant: 12).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 10).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -10).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        recentTableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10).isActive = true
        recentTableView.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        recentTableView.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        recentTableView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        
       

    }
    
  
    
   
    
    
    
}

// MARK: - UITableViewDataSource
extension SearchRecentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let recent = self.recentSeachWord else{return 0}
        return recent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchRecentListTableViewCell.self)
        
        let delButton = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        delButton.setImage(UIImage(named: "icoDel-1"), for: .normal)
        delButton.tag = indexPath.row
        delButton.addTarget(self, action: #selector(didClickDeleteCellButton(_ :)), for: .touchUpInside)
        
        cell.accessoryView = delButton
        cell.selectionStyle = .none
        
        
        guard let recent = self.recentSeachWord?[indexPath.row] else{return cell}
        cell.configure(title: recent)
        
        return cell
    }
    
    
}

// MARK: - UITableViewDelegate
extension SearchRecentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        
        let label = BaseLabel()
        label.configure(font: .godo_M(14), color: .rgb(136))
        label.text = "Recent Searches"
        
        view.addSubview(label)
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let recent = self.recentSeachWord?[indexPath.row] else{return}
        
        let vc = SearchResultViewController(text:recent.lowercased(), searchCode: String(1))
        show(vc)
   //w
    }
    
}

// MARK: - UITextFieldDelegate
extension SearchRecentViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let searchWord = textField.text else{return true}
        guard var recent = recentSeachWord else{
            var recentArray = [String]()
            recentArray.append(searchWord)
            recentSearchWordx = recentArray
            self.recentSeachWord = recentSearchWordx
            self.recentTableView.reloadData()
            return true
        }
        if recent.contains(searchWord) {
            guard let index = recent.firstIndex(of: searchWord) else{return true}
            recent.remove(at: index)
            recent.insert(searchWord, at: 0)
            recentSearchWordx = recent
            self.recentSeachWord = recentSearchWordx
            self.recentTableView.reloadData()
        } else{
            if recent.count >= 5 {
                recent.removeLast()
                recent.insert(searchWord, at: 0)
                recentSearchWordx = recent
                self.recentSeachWord = recentSearchWordx
                self.recentTableView.reloadData()
            } else {
                recent.insert(searchWord, at: 0)
                recentSearchWordx = recent
                self.recentSeachWord = recentSearchWordx
                self.recentTableView.reloadData()
            }
        }
        textField.text = nil
        
        textField.resignFirstResponder()
        
        
    
        
        return true
    }

    
}
// MARK: - UISearchBarDelegate
extension SearchRecentViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let searchWord = searchBar.text else{return}
        guard var recent = recentSearchWordx else{
            var recentArray = [String]()
            recentArray.append(searchWord)
            recentSearchWordx = recentArray
            let vc = SearchResultViewController(text:searchBar.text, searchCode: String(1))
            show(vc)
          //  getInfo(searchText: searchWord, page: page)
            return
        }
        if recent.contains(searchWord) {
            guard let index = recent.firstIndex(of: searchWord) else{return}
            recent.remove(at: index)
            recent.insert(searchWord, at: 0)
            recentSearchWordx = recent
            let vc = SearchResultViewController(text:searchBar.text, searchCode: String(1))
            show(vc)
           // getInfo(searchText: searchWord, page: page)
        } else {
            if recent.count >= 5 {
                recent.removeLast()
                recent.insert(searchWord, at: 0)
                recentSearchWordx = recent
                let vc = SearchResultViewController(text:searchBar.text, searchCode: String(1))
                show(vc)
              //  getInfo(searchText: searchWord, page: page)
            } else {
                recent.insert(searchWord, at: 0)
                recentSearchWordx = recent
                let vc = SearchResultViewController(text:searchBar.text, searchCode: String(1))
                show(vc)
              //  getInfo(searchText: searchWord, page: page)
            }
        }
       // self.searchText = searchWord
      //  searchBar.text = nil

    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      //  searchBar.text = nil
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
