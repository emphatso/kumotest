//
//  MainService.swift
//  LetsBee
//
//  Created by WonJun Choi on 2020/07/03.
//  Copyright © 2020 WonJun Choi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MainService {
    
    func getItunesData(completion: @escaping
                            ((_ reponsedataOBJ: ItunesData) -> ()) ) {
    
        var responseDataOBJ = ItunesData()

        Alamofire.request("https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all", method: .get).responseDecodable{ (response: DataResponse<ItunesData>) in
          
            switch response.result {
            case .failure:
                responseDataOBJ.resultCount = 0
                print("Failure Response: ", response.result)
            case .success:
                let json = JSON(response.data)
                
                let data = json["results"].array?.map{ItunesDataModels($0)}
                iTunesDatas = data!
                
            if let data = response.result.value {
                responseDataOBJ.results = data.results
               
                print("Response: ", responseDataOBJ)
                }
           }
            completion(responseDataOBJ)
        }.resume()
    }
    
  

}
