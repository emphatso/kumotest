//
//  SearchService.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/7/22.
//

import UIKit
import SwiftyJSON
import Alamofire

class SearchService {
    func getSearchResult(searchText:String, completion: @escaping
                            ((_ reponsedataOBJ: ItunesData) -> ()) ) {
    
        var responseDataOBJ = ItunesData()

        Alamofire.request("https://itunes.apple.com/search?term=\(searchText)", method: .get).responseDecodable{ (response: DataResponse<ItunesData>) in
          
            switch response.result {
            case .failure:
                responseDataOBJ.resultCount = 0
                print("Failure Response: ", response.result)
            case .success:
            if let data = response.result.value {
                responseDataOBJ.results = data.results
                print("Response: ", responseDataOBJ)
                }
           }
            completion(responseDataOBJ)
        }.resume()
    }
}
