//
//  SplashViewController.swift
//  KUMOTEST
//
//  Created by John Carpio on 2/5/22.
//

import UIKit

class SplashViewController: BaseViewController {
    // MARK: - Properties
    var versionCheck = false
    
    // MARK: - UI Component
    
    private let ciImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "Apple")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
  
    
    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
        view.addSubview(ciImageView)
        layout()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
      
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(viewControllerVisited,"SHEE")
        switch viewControllerVisited {
         
        case "Favorite":
            let vc = MainViewController()
          
            vc.animateToTab(toIndex: 2)
            vc.setIndex(index: 2)
            self.show(vc)
        case "Music":
            let vc = MainViewController()
            vc.animateToTab(toIndex: 0)
            vc.setIndex(index: 0)
            self.show(vc)
        case "Movies":
            let vc = MainViewController()
            vc.animateToTab(toIndex: 1)
            vc.setIndex(index: 1)
            self.show(vc)
        case "Search":
            let vc = MainViewController()
            vc.animateToTab(toIndex: 3)
            vc.setIndex(index: 3)
            self.show(vc)
        default:
            let vc = MainViewController()
            vc.animateToTab(toIndex: 0)
            vc.setIndex(index: 0)
            self.show(vc)
        }
        
        if versionCheck {
//            if isLoginx || isKakaoLoginx || isAppleLoginx || isGoogleLoginx || mkeyx != 0 {
//                self.setLogin()
//            } else {
//                self.showStartViewController()
//            }
        }

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        setNavigationBar(isUnderline: true, barTintColor: .beeYellow)
        setStatusBar(color: .beeYellow)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    // MARK: - Private Method
    private func initialSetting() {

        view.backgroundColor = .clear
        
    }
    
    private func showMainViewController() {

    }
    private func showStartViewController() {
    
        
    }
 
    
    // MARK: - Network
  
  
    //MARK: - Layout
    private func layout() {
        ciImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        ciImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        ciImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        ciImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

